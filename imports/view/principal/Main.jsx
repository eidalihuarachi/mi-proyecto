import React, { Component } from 'react';
import{publicationsClass} from'../../models/publications/class'
import { withTracker } from 'meteor/react-meteor-data' 
import {categoryClass} from '/imports/models/category/class' 

class Main extends Component {
    render() {
        const {publications,subscriptionPublications,categorys}= this.props
       // console.log(subscriptionPublications.ready())
       // console.log(categorys)
        return (
            <div>

                <section className="hero">
                    <div className="hero__slider owl-carousel">
                        <div className="hero__item set-bg" data-setbg="/principal/img/hero/hero-1.jpg">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="hero__text">
                                            <span>PLATAFORMA DINAMICA DE MUSISCOS BOLIVIANOS</span>
                                            <h2>TEAM BOLIVA</h2>
                                            <img src="/principal/img/grupos/KJARKAS.jpg" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="hero__item set-bg" data-setbg="/principal/img/hero/hero-1.jpg">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="hero__text">
                                            <span>PLATAFORMA DINAMICA DE MUSISCOS BOLIVIANOS</span>
                                            <h2>TEAM BOLIVA</h2>
                                            <img src="/principal/img/grupos/PASION ANDINA111.jpg" alt="" style={{}} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="hero__item set-bg" data-setbg="/principal/img/hero/hero-1.jpg">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="hero__text">
                                            <span>PLATAFORMA DINAMICA DE MUSISCOS BOLIVIANOS</span>
                                            <h2>TEAM BOLIVA</h2>
                                            <img src="/principal/img/grupos/CHILA JATUN.jpg" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="team spad set-bg" data-setbg="/principal/img/team-bg.jpg">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="section-title team__title">
                                    <span>EVENTOS RECIENTES</span>
                                    <h2>BOLIVIA</h2>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-3 col-md-6 col-sm-6 p-0">
                                <div className="team__item set-bg" data-setbg="/principal/img/grupos/KJARKAS.jpg">
                                    <div className="team__item__text">
                                        <h4>kjarkas</h4>
                                        <p>CONCIERTO</p>
                                        <div className="team__item__social">
                                            <a href="#"><i className="fa fa-facebook"></i></a>
                                            <a href="#"><i className="fa fa-twitter"></i></a>
                                            <a href="#"><i className="fa fa-dribbble"></i></a>
                                            <a href="#"><i className="fa fa-instagram"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-6 col-sm-6 p-0">
                                <div className="team__item team__item--second set-bg"
                                    data-setbg="/principal/img/grupos/alcoholica.jpg">
                                    <div className="team__item__text">
                                        <h4>ALCOHOLICA</h4>
                                        <p>TEAM BOLIVIA</p>
                                        <div className="team__item__social">
                                            <a href="#"><i className="fa fa-facebook"></i></a>
                                            <a href="#"><i className="fa fa-twitter"></i></a>
                                            <a href="#"><i className="fa fa-dribbble"></i></a>
                                            <a href="#"><i className="fa fa-instagram"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-6 col-sm-6 p-0">
                                <div className="team__item team__item--third set-bg"
                                    data-setbg="/principal/img/grupos/HISTERIA.jpg">
                                    <div className="team__item__text">
                                        <h4>HISTERIA</h4>
                                        <p>Videographer</p>
                                        <div className="team__item__social">
                                            <a href="#"><i className="fa fa-facebook"></i></a>
                                            <a href="#"><i className="fa fa-twitter"></i></a>
                                            <a href="#"><i className="fa fa-dribbble"></i></a>
                                            <a href="#"><i className="fa fa-instagram"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-6 col-sm-6 p-0">
                                <div className="team__item team__item--four set-bg"
                                    data-setbg="/principal/img/grupos/proyeccion.jpg">
                                    <div className="team__item__text">
                                        <h4>PROYECCION</h4>
                                        <p>Videographer</p>
                                        <div className="team__item__social">
                                            <a href="#"><i className="fa fa-facebook"></i></a>
                                            <a href="#"><i className="fa fa-twitter"></i></a>
                                            <a href="#"><i className="fa fa-dribbble"></i></a>
                                            <a href="#"><i className="fa fa-instagram"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-12 p-0">
                                <div className="team__btn">
                                    <a href="#" className="primary-btn">Meet Our Team</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>

                
                <section className="portfolio spad">
                   
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <ul className="portfolio__filter">
                                    <li className="active" data-filter="*">TODOS</li>
                                    {
                                        categorys.map((data,key)=>{
                                            return <li data-filter=".branding">{data.name} </li>
                                        })
                                    }
                                
                                </ul>
                            </div>
                        </div>
                        <div className="row portfolio__gallery" id="MixItUp59DCAB">
                        {
                        publications.map((data,key)=>{

                        return <div className="col-lg-4 col-md-6 col-sm-6 mix branding">
                            <div className="portfolio__item">
                                {data.typepub =='video'?
                                    (
                                        <video width={380} height={280} controls>
                                            <source src={data.urlfile} />
                                            Your browser does not support the video tag.
                                        </video>
                                    )
                                     :
                                     <div className="portfolio__item__video set-bg" data-setbg={data.urlfile}
                                    style={{backgroundImage: `url("${data.urlfile}")`}}>
                                   
                                </div>
                                }
                                
                                <div className="portfolio__item__text">
                                    <h4>{data.title}</h4>
                                    <ul>
                                        <li>{data.username} </li>
                                        <li>Contacto:{data.phone}</li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        })
                        }
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="pagination__option">
                                    <a href="#" className="arrow__pagination left__arrow"><span
                                            className="arrow_left" /> Prev</a>
                                    <a href="#" className="number__pagination">1</a>
                                    <a href="#" className="number__pagination">2</a>
                                    <a href="#" className="arrow__pagination right__arrow">Next <span
                                            className="arrow_right" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="callto spad set-bg" data-setbg="/principal/img/callto-bg.jpg">
                    <div className="container">
                        <div className="row" />
                        <div className="col-lg-8">
                            <div className="callto__text">
                                <h2>Fresh Ideas, Fresh Moments Giving Wings to your Stories.</h2>
                                <p>INC5000, Best places to work 2019</p>
                                <a href="#">Start your stories</a>
                            </div>
                        </div>
                    </div>

                </section>




            </div>
        );
    }
}



export default withTracker((props)=>{
    const subscriptionPublications = Meteor.subscribe('publications',{},'getAllPublications')
   // console.log(subscriptionPublications)
    const publications = publicationsClass.find({},{sord:{createdAt:-1}}).fetch()
    //console.log(publications)
    const subcriptionCategory = Meteor.subscribe('category',{},'getAllCategory')
    //console.log(subcriptionCategory.ready())
    const categorys = categoryClass.find().fetch()
    return{publications,subscriptionPublications,categorys,subcriptionCategory}
})(Main)
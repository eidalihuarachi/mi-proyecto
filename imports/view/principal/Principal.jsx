import React, { Component } from 'react';
import {Link,Switch} from 'react-router-dom'
import SwitchRoutes from '/imports/routes/SwitchRoutes'
import Nav from '/imports/components/Nav'

class Principal extends Component {
componentDidMount(){
import '/imports/assets/principal/css'
import '/imports/assets/principal/js/'
}
render() {
const {routes} = this.props     
return (
    <div>
        <div>
            {/*<div id="preloder">
                <div className="loader"></div>
            </div>*/}


            <header className="header">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-2">
                            <div className="header__logo">
                                <a href="./index.html"><img src="/principal/img/logo.png" alt="" /></a>
                            </div>
                        </div>
                        <div className="col-lg-10">
                            <div className="header__nav__option">
                                <nav className="header__nav__menu mobile-menu">
                                     <ul>
                                        <li className="active"><a href="./index.html">PRINCIPAL</a></li>
                                        <li><a href="./about.html">About</a></li>
                                        <li><a href="./portfolio.html">Portfolio</a></li>
                                        <li><a href="./services.html">Services</a></li>
                                        <li><a href="/bienvenido/registro">registrarme</a></li>
                                        <li><a href="/bienvenido/login">iniciar seccion</a></li>
                                    </ul>
                                </nav>
                                <div className="header__nav__social">
                                    <a href="#"><i className="fa fa-facebook"></i></a>
                                    <a href="#"><i className="fa fa-twitter"></i></a>
                                    <a href="#"><i className="fa fa-dribbble"></i></a>
                                    <a href="#"><i className="fa fa-instagram"></i></a>
                                    <a href="#"><i className="fa fa-youtube-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="mobile-menu-wrap"></div>
                </div>
            </header>
            <main>
                <Switch>
                    {
                    routes.map((route,i)=>(
                        <SwitchRoutes key={i} {...route} />
                    ))
                    }
                </Switch>

            </main>


            <footer className="footer">
                <div className="container">
                    <div className="footer__top">
                        <div className="row">
                            <div className="col-lg-6 col-md-6">
                                <div className="footer__top__logo">
                                    <a href="#"><img src="/principal/img/logo.png" alt="" /></a>
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6">
                                <div className="footer__top__social">
                                    <a href="#"><i className="fa fa-facebook"></i></a>
                                    <a href="#"><i className="fa fa-twitter"></i></a>
                                    <a href="#"><i className="fa fa-dribbble"></i></a>
                                    <a href="#"><i className="fa fa-instagram"></i></a>
                                    <a href="#"><i className="fa fa-youtube-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="footer__option">
                        <div className="row">
                            <div className="col-lg-4 col-md-6 col-sm-6">
                                <div className="footer__option__item">
                                    <h5>About us</h5>
                                    <p>Formed in 2006 by Matt Hobbs and Cael Jones, Videoprah is an award-winning,
                                        full-service
                                        production company specializing.</p>
                                    <a href="#" className="read__more">Read more <span
                                            className="arrow_right"></span></a>
                                </div>
                            </div>
                            <div className="col-lg-2 col-md-3 col-sm-3">
                                <div className="footer__option__item">
                                    <h5>Who we are</h5>
                                    <ul>
                                        <li><a href="#">Team</a></li>
                                        <li><a href="#">Carrers</a></li>
                                        <li><a href="#">Contact us</a></li>
                                        <li><a href="#">Locations</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-lg-2 col-md-3 col-sm-3">
                                <div className="footer__option__item">
                                    <h5>Our work</h5>
                                    <ul>
                                        <li><a href="#">Feature</a></li>
                                        <li><a href="#">Latest</a></li>
                                        <li><a href="#">Browse Archive</a></li>
                                        <li><a href="#">Video for web</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-12">
                                <div className="footer__option__item">
                                    <h5>Newsletter</h5>
                                    <p>Videoprah is an award-winning, full-service production company specializing.</p>
                                    <form action="#">
                                        <input type="text" placeholder="Email" />
                                        <button type="submit"><i className="fa fa-send"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="footer__copyright">
                        <div className="row">
                            <div className="col-lg-12 text-center">

                                <p className="footer__copyright__text">Copyright &copy;
                                    <script>
                                        document.write(new Date().getFullYear());
                                    </script>
                                    All rights reserved | This template is made with <i className="fa fa-heart-o"
                                        aria-hidden="true"></i> by <a href="https://colorlib.com"
                                        target="_blank">Colorlib</a>
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </footer>


        </div>
    </div>
);
}
}

export default Principal;
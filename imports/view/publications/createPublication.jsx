import React, { Component } from 'react';
import {publicationsClass} from '../../models/publications/class'
import {categoryClass} from '../../models/category/class'
import {Meteor} from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data' 
import DatePicker from "react-datepicker";
import FileUpload from '../../components/FileUpload'
import uploadFiles from '../../utils/upload'


class createPublication extends Component {
    constructor(props){
        super(props)
        this.state={
            form:{
                title:null,
                description:null,
                phone:null,
                startDate:new Date(),
                image:null,
                category:null
            },
            typefile:'image',
            videouri:null,
            errors:{}
        }
    }
    validateForm=() =>{                     //validaciones del formulario
        const {form} = this.state
        let errorsform = {}
        let formsIsValid = true
        if(!form.title){
            formsIsValid = false
            errorsform.title = "el titulo no puede estar vacio"
        }
        if(!form.description){
            formsIsValid = false
            errorsform.description = "la descripcion no puede estar vacio"
        }
        if(!form.phone){
            formsIsValid = false
            errorsform.phone = "el telefono no puede estar vacio"
        }
        if(!form.startDate){
            formsIsValid = false
            errorsform.startDate = "la fecha no puede estar vacio"
        }
        
        this.setState({errors:errorsform})
        return formsIsValid
    }
    //createNewPublications(e){ 
    createNewPublications=(e)=>{ // tiene acceso a las variables de esta forma
           e.preventDefault()
           if(this.validateForm()){
                //alert('ENVIANDO FORMULARIO')
                //console.log(this.state.form)
                const newpublicacion = new publicationsClass()  //llamar metodo
                const {form}= this.state                   // obtenet la propiedad
                console.log(form)
                const {form:{image}} = this.state
                const uf = new uploadFiles(image.file,image.self)
                const mthis = this
                uf.newUpload(function(error,success){
                    if(error){
                       console.log('**********************************')
                       console.log(error)
                       console.log('**********************************')
                       }else{
                             form.image = success._id
                             newpublicacion.callMethod('newPublication',form,(error,result)=>{  //extencion de la clase hacia el servidor
                                 if(error){
                                    alert(error)
                                }else{
                                   alert(result)
                                   document.getElementById("newPublication").reset()
                                   mthis.setState({typefile:'image'})
                                }
                           })
                       }
                   })
                   /////
           }else{
              alert('EL FORMULARIO TIENE ERRORES')
           }
    }
    chargeTextInput = (e) => {
        const values = e.target.value
        const property = e.target.name
        this.setState(prevState=>(
            {form:{
                   ...prevState.form,
                   [property]:values,    // propiedad dinamica del metodo
                  }
            }
        ))
    }
    changeSelectInput = (e)=>{
        const value = e.target.value
        this.setState(prevState=>(
            {form:{
                   ...prevState.form,
                  category:value,    
                  }
            }
        ))
    }
    changeFileInput =(data)=>{
        //console.log(data)
        const inputfile = data.file

        if(inputfile && inputfile[0]){
            const Indextext = inputfile[0].type.lastIndexOf('/')
            const typefile = inputfile[0].type.slice(0,Indextext)
            if(typefile == 'image'){
                this.setState({typefile:'image'})
                let reader = new FileReader()
                reader.onload = function(v){
                     $('#previewimage').attr('src',v.target.result)
                }
                reader.readAsDataURL(inputfile[0])


            }else{
                this.setState({typefile:'video'})
                let file = inputfile[0]
                let blobURL = URL.createObjectURL(file)
                console.log(blobURL)
                this.setState({videouri:blobURL})
                this.setState({typefile:'video'})
                //$('previewvideo').src = blobURL
            }
            this.setState(prevState=>(
                {form:{
                        ...prevState.form,
                        image:data,    // propiedad dinamica del metodo
                    }
               }
            ))
            
        }
        
    }
    render() {
        const {errors,typefile,videouri} = this.state
        const {categorys,subcriptionCategory} = this.props
        return (
            <div>
                <section className="section">
                    <div className="section-body">
                        <div className="row">
                            <div className="col-12 col-md-12 col-lg-12">
                                <div className="card">
                                    <div className="card-header">
                                        <h4>--------------NUEVA PUBLICACION DE VIDEOCLIP----------------</h4>
                                    </div>
                                    {!subcriptionCategory.ready()?
                                         <h1>CARGANDO¡¡¡¡</h1>
                                     :
                                     <div className="card-body">
                                         
                                     <form onSubmit={this.createNewPublications} id="newPublication">
                                         <div className="row">
                                              <div className="col-md-6">
                                               <div className="form-group">
                                             <label>TITULO DE LA CANCION</label>
                                             <input type="text" className={errors.title?"form-control is-invalid":"form-control"} name={'title'} onChange={this.chargeTextInput} autoComplete="off"/>
                                             {
                                                 errors.title?
                                                       <div className="invalid-feedback">
                                                           {errors.title}
                                                       </div>
                                                 :
                                                       null
                                             }
                                         </div>
                                         <div className="form-group">
                                             <label>AUTORIA DE LA CANCION</label>
                                             <textarea type="text" className={errors.description?"form-control invoice-input is-invalid":"form-control invoice-input"} name={'description'} onChange={this.chargeTextInput} autoComplete="off">  
                                             </textarea>
                                             {
                                                 errors.description?
                                                       <div className="invalid-feedback">
                                                           {errors.description}
                                                       </div>
                                                    :null
                                             }
                                         </div>
                                         <div className="form-group">
                                             <label>GRUPO</label>
                                             <textarea type="text" className={errors.description?"form-control invoice-input is-invalid":"form-control invoice-input"} name={'description'} onChange={this.chargeTextInput} autoComplete="off">  
                                             </textarea>
                                             {
                                                 errors.description?
                                                       <div className="invalid-feedback">
                                                           {errors.description}
                                                       </div>
                                                    :null
                                             }
                                         </div>
                                         <div className="form-group">
                                             <label>CONTACTO TELEFONICO</label>
                                             <div className="input-group">
                                                 <div className="input-group-prepend">
                                                     <div className="input-group-text">
                                                         <i className="fas fa-phone"></i>
                                                     </div>
                                                 </div>
                                                 <input type="text" className={errors.phone?"form-control phone-number is-invalid":"form-control phone-number"} name={'phone'} onChange={this.chargeTextInput} autoComplete="off"/>
                                                 {
                                                     errors.phone?
                                                         <div className="invalid-feedback">
                                                             {errors.phone}
                                                         </div>
                                                     :
                                                         null
                                                 }
                                             </div>
                                         </div>
                                         <div className="form-group">
                                             <label>SELECCIONE UN GENERO MUSICAL</label>
                                             <select className="form-control" onChange={this.changeSelectInput}>
                                             {
                                                 categorys.map((category,key)=>{
                                                     return <option key={`category ${key}`} value={category._id}>{category.name}</option>
                                                 })
                                              }
                                            </select>
                                         </div>
                                         <div className="form-group">
                                             <label>ARCHIVO</label>
                                             {/*<input type="file" className='form-contro' name={'file'} onChange={this.changeFileInput}></input>*/}
                                             <FileUpload changeFileInput={this.changeFileInput} accept=".jpeg, .jpg, .png, .mp4" />
                                         </div>
 
                                         <div className="form-group">
                                             <label>FECHA DE ESTRENO</label>
                                             { /*<input type="text" className={errors.startDate?"form-control datemask is-invalid":"form-control datemask"} placeholder="YYYY/MM/DD" name={'startDate'} onChange={this.chargeTextInput} autoComplete="off"/>*/ }
                                             <div>
                                               <DatePicker selected={this.state.form.startDate} className={errors.startDate?"form-control datemask is-invalid":"form-control datemask"} name={'startDate'} 
                                               onChange={(date) => {
                                                this.setState(prevState=>(
                                                    {form:{
                                                           ...prevState.form,
                                                           startDate:date,    // propiedad dinamica del metodo
                                                          }
                                                    }
                                                ))
                                               } }
                                               />
                                             {
                                                     errors.startDate?
                                                         <div className="invalid-feedback">
                                                             {errors.startDate}
                                                         </div>
                                                     :
                                                         null
                                             }
                                             </div>
                                             
                                         </div>
                                         
                                         
                                              </div>
                                              <div className="col-md-6">
                                              <div className="author-box-center d-flex justify-content-center">
                                                  {typefile=='image'?
                                                      <img alt="image" src="/dashboard/img/users/iconn.png" id="previewimage" className="rounded-circle author-box-picture" style={{width:'100%',height:'500'}} />
                                                  :
                                                      <video width={380} height={280} id="previewvideo" controls>
                                                         <source src={videouri} />
                                                      </video>
                                                  }
                                                 
                                                 <div className="clearfix"></div>
                                                 
                                                 <div className="author-box-job d-flex justify-content-center"></div>
                                             </div>
                                              </div>
                                         </div>     
                                         <button type="submit" className="btn btn-icon icon-left btn-primary"><i className="far fa-edit"></i>PUBLICAR</button>
                                     </form>
                                 </div>
                                    }
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default withTracker((props)=>{
      const subcriptionCategory = Meteor.subscribe('category',{},'getCategory')
      //console.log(subcriptionCategory.ready())
      const categorys = categoryClass.find().fetch()
      return {categorys,subcriptionCategory}
})(createPublication)
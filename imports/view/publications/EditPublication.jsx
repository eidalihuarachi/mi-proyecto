import React, { Component } from 'react';
import {publicationsClass} from '../../models/publications/class'
import {categoryClass} from '../../models/category/class'
import {Meteor} from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data' 
import DatePicker from "react-datepicker";
import FileUpload from '../../components/FileUpload'
import uploadFiles from '../../utils/upload'


class EditPublication extends Component {
    constructor(props){
        super(props)
        this.state={
            form:{
                title:props.getpublication.title,
                description:props.getpublication.description,
                phone:props.getpublication.phone,
                startDate:new Date(),
                image:null,
                category:null
            },
 
            errors:{}
        }
    }
    validateForm=() =>{                     //validaciones del formulario
        const {form} = this.state
        let errorsform = {}
        let formsIsValid = true
        if(!form.title){
            formsIsValid = false
            errorsform.title = "el titulo no puede estar vacio"
        }
        if(!form.description){
            formsIsValid = false
            errorsform.description = "la descripcion no puede estar vacio"
        }
        if(!form.phone){
            formsIsValid = false
            errorsform.phone = "el telefono no puede estar vacio"
        }
        if(!form.startDate){
            formsIsValid = false
            errorsform.startDate = "la fecha no puede estar vacio"
        }
        
        this.setState({errors:errorsform})
        return formsIsValid
    }
    //createNewPublications(e){ 
    createNewPublications=(e)=>{ // tiene acceso a las variables de esta forma
           e.preventDefault()
           if(this.validateForm()){
                //alert('ENVIANDO FORMULARIO')
                //console.log(this.state.form)
                const newpublicacion = new publicationsClass()  //llamar metodo
                const {form}= this.state                   // obtenet la propiedad
                console.log(form)
                const {form:{image}} = this.state
                const uf = new uploadFiles(image.file,image.self)
                uf.newUpload(function(error,success){
                    if(error){
                       console.log('**********************************')
                       console.log(error)
                       console.log('**********************************')
                       }else{
                             form.image = success._id
                             newpublicacion.callMethod('newPublication',form,(error,result)=>{  //extencion de la clase hacia el servidor
                                 if(error){
                                    alert(error)
                                }else{
                                   alert(result)
                                   document.getElementById("newPublication").reset()
                                }
                           })
                       }
                   })
                   /////
           }else{
              alert('EL FORMULARIO TIENE ERRORES')
           }
    }
    chargeTextInput = (e) => {
        const values = e.target.value
        const property = e.target.name
        this.setState(prevState=>(
            {form:{
                   ...prevState.form,
                   [property]:values,    // propiedad dinamica del metodo
                  }
            }
        ))
    }
    changeSelectInput = (e)=>{
        const value = e.target.value
        this.setState(prevState=>(
            {form:{
                   ...prevState.form,
                  category:value,    
                  }
            }
        ))
    }
    changeFileInput =(data)=>{
        //console.log(data)
        const inputfile = data.file

        if(inputfile && inputfile[0]){
            let reader = new FileReader()
            reader.onload = function(v){
                $('#previewimage').attr('src',v.target.result)
            }
            reader.readAsDataURL(inputfile[0])

            this.setState(prevState=>(
                {form:{
                       ...prevState.form,
                       image:data,    // propiedad dinamica del metodo
                      }
                }
            ))
        }
        
    }
    render() {
        const {errors} = this.state
        const {categorys,subcriptionCategory,getpublication,subcriptionPublication} = this.props
        console.log(getpublication)
        return (
            <div>
                <section className="section">
                    <div className="section-body">
                        <div className="row">
                            <div className="col-12 col-md-12 col-lg-12">
                                <div className="card">
                                    <div className="card-header">
                                        <h4>--------------EDITAR PUBLICACION DE VIDEOCLIP----------------</h4>
                                    </div>
                                    {!subcriptionCategory.ready() && subcriptionPublication.ready()?
                                         <h1>CARGANDO¡¡¡¡</h1>
                                     :
                                     <div className="card-body">
                                         
                                     <form onSubmit={this.createNewPublications} id="newPublication">
                                         <div className="row">
                                              <div className="col-md-6">
                                               <div className="form-group">
                                             <label>TITULO DE LA CANCION</label>
                                             <input type="text" className={errors.title?"form-control is-invalid":"form-control"} value={getpublication.title} name={'title'} onChange={this.chargeTextInput} autoComplete="off"/>
                                             {
                                                 errors.title?
                                                       <div className="invalid-feedback">
                                                           {errors.title}
                                                       </div>
                                                 :
                                                       null
                                             }
                                         </div>
                                         <div className="form-group">
                                             <label>AUTORIA DE LA CANCION</label>
                                             <textarea type="text" value={getpublication.description} className={errors.description?"form-control invoice-input is-invalid":"form-control invoice-input"} name={'description'} onChange={this.chargeTextInput} autoComplete="off">  
                                             </textarea>
                                             {
                                                 errors.description?
                                                       <div className="invalid-feedback">
                                                           {errors.description}
                                                       </div>
                                                    :null
                                             }
                                         </div>
                                         <div className="form-group">
                                             <label>GRUPO</label>
                                             <textarea type="text" className={errors.description?"form-control invoice-input is-invalid":"form-control invoice-input"} name={'description'} onChange={this.chargeTextInput} autoComplete="off">  
                                             </textarea>
                                             {
                                                 errors.description?
                                                       <div className="invalid-feedback">
                                                           {errors.description}
                                                       </div>
                                                    :null
                                             }
                                         </div>
                                         <div className="form-group">
                                             <label>CONTACTO TELEFONICO</label>
                                             <div className="input-group">
                                                 <div className="input-group-prepend">
                                                     <div className="input-group-text">
                                                         <i className="fas fa-phone"></i>
                                                     </div>
                                                 </div>
                                                 <input type="text" value={getpublication.phone} className={errors.phone?"form-control phone-number is-invalid":"form-control phone-number"} name={'phone'} onChange={this.chargeTextInput} autoComplete="off"/>
                                                 {
                                                     errors.phone?
                                                         <div className="invalid-feedback">
                                                             {errors.phone}
                                                         </div>
                                                     :
                                                         null
                                                 }
                                             </div>
                                         </div>
                                         <div className="form-group">
                                             <label>SELECCIONE UN GENERO MUSICAL</label>
                                             <select className="form-control" value={getpublication.nameCategory} onChange={this.changeSelectInput}>
                                                 <option> seleccione una categoria </option>
                                             {
                                                 categorys.map((category,key)=>{
                                                     return <option key={`category ${key}`} value={category._id}>{category.name}</option>
                                                 })
                                              }
                                            </select>
                                         </div>
                                         <div className="form-group">
                                             <label>ARCHIVO</label>
                                             {/*<input type="file" className='form-contro' name={'file'} onChange={this.changeFileInput}></input>*/}
                                             <FileUpload changeFileInput={this.changeFileInput} />
                                         </div>
 
                                         <div className="form-group">
                                             <label>FECHA DE ESTRENO</label>
                                             { /*<input type="text" className={errors.startDate?"form-control datemask is-invalid":"form-control datemask"} placeholder="YYYY/MM/DD" name={'startDate'} onChange={this.chargeTextInput} autoComplete="off"/>*/ }
                                             <div>
                                               <DatePicker selected={getpublication.startDate} className={errors.startDate?"form-control datemask is-invalid":"form-control datemask"} name={'startDate'} 
                                               onChange={(date) => {
                                                this.setState(prevState=>(
                                                    {form:{
                                                           ...prevState.form,
                                                           startDate:date,    // propiedad dinamica del metodo
                                                          }
                                                    }
                                                ))
                                               } }
                                               />
                                             {
                                                     errors.startDate?
                                                         <div className="invalid-feedback">
                                                             {errors.startDate}
                                                         </div>
                                                     :
                                                         null
                                             }
                                             </div>
                                             
                                         </div>
                                         
                                         
                                              </div>
                                              <div className="col-md-6">
                                              <div className="author-box-center d-flex justify-content-center">
                                                 <img alt="image" src={getpublication.urlfile} id="previewimage" className="rounded-circle author-box-picture" style={{width:'100%',height:'500'}} />
                                                 <div className="clearfix"></div>
                                                 
                                                 <div className="author-box-job d-flex justify-content-center"></div>
                                             </div>
                                              </div>
                                         </div>     
                                         <button type="submit" className="btn btn-icon icon-left btn-primary"><i className="far fa-edit"></i>PUBLICAR</button>
                                     </form>
                                 </div>
                                    }
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default withTracker((props)=>{
    const {location:{state:{publication}}} = props
    const subcriptionPublication = Meteor.subscribe('publications',{pub:publication},'getOnePublication')
    const subcriptionCategory = Meteor.subscribe('category',{},'getCategory')
    const categorys = categoryClass.find().fetch()
    const getpublication = publicationsClass.findOne()
    console.log(subcriptionPublication.ready())
    console.log(getpublication)
    return {categorys,subcriptionCategory,getpublication,subcriptionPublication}
})(EditPublication)

import React, { Component } from 'react';
import {Switch} from 'react-router-dom'
import SwitchRoutes from '/imports/routes/SwitchRoutes'
import Nav from '/imports/components/Nav' 
import Sidebar from '/imports/components/Sidebar'
import Home from '/imports/view/home/Home'
import { Meteor } from 'meteor/meteor';
const feather = require('feather-icons')
import "react-datepicker/dist/react-datepicker.css";
import {Roles} from 'meteor/alanning:roles'

import '/imports/assets/dashboard/css/'
import '/imports/assets/dashboard/js/'
feather.replace();

class Layout extends Component {
    render() {
        const {routes} = this.props
        return (
            <div>
                <div className="loader"></div>
                <div id="app">
                    <div className="main-wrapper main-wrapper-1">
                        <div className="navbar-bg"></div>
                        <Nav/>
                        <Sidebar/>
                        <div className="main-content">
                        <Switch>
                            {
                            routes.map((route,i)=>{ 
                                if(route.permission){
                                    if(Roles.userIsInRole(Meteor.userId(),route.permission)){
                                        console.log('acceso')
                                        return <SwitchRoutes key={i} {...route} />
                                   }else{
                                       console.log('acceso denegado')
                                    }
                                    
                               }else{
                                    return <SwitchRoutes key={i} {...route} />
                                }
                                
                            })
                            }
                        </Switch>
                        </div>
                        <footer className="main-footer">
                            <div className="footer-left">
                                <a href="templateshub.net">Templateshub</a>
                            </div>
                            <div className="footer-right">
                            </div>
                        </footer>
                    </div>
                </div>

            </div>
        );
    }
}

export default Layout;
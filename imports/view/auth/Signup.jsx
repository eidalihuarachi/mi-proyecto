import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';

class Signup extends Component {
    constructor(props){
        super(props)
        this.state={
        nameComplete:'',
            password:'',
            email:'',
            username:'',
            repassword:''
        }
    }
    SubmitForRegister = (e) =>{
        e.preventDefault()
        const form = {
            nameComplete:this.state.nameComplete,
            password:this.state.password,
            email:this.state.email,
            username:this.state.username,
            repassword:this.state.repassword
        }
        Meteor.call('userCreate',form,function(error,resp){
            if(error){
                alert(error.reason)
            }else{
                alert(resp)
                Meteor.loginWithPassword(form.username,form.password,function(error){
                    if (error)
                        console.log(error)
                    else{
                        //mthis.props.history.push('/dashboard/home')
                        window.location.replace('/dashboard/home')
                    }
                })
            }     
        }) 
    }
    render() {
        return (
            <div className="form">
                <div className="tab-content">
                    <div id="signup">
                        <h1>Registro de Usuario</h1>

                        <form onSubmit = {this.SubmitForRegister}>

                            <div className="top-row">
                                <div className="field-wrap">
                                    <label>
                                        Nombre Completo<span className="req"></span>
                                    </label>
                                    <input type="text" required autoComplete="off"onChange={e=> this.setState({nameComplete:e.target.value})} />
                                </div>

                                <div className="field-wrap">
                                    <label>
                                        Nombre de Usuario<span className="req"></span>
                                    </label>
                                    <input type="text" required autoComplete="off" onChange={e=> this.setState({username:e.target.value})} />
                                </div>
                            </div>

                            <div className="field-wrap">
                                <label>
                                    Correo Electronico<span className="req"></span>
                                </label>
                                <input type="email" required autoComplete="off" onChange={e=> this.setState({email:e.target.value})} />
                            </div>

                            <div className="field-wrap">
                                <label>
                                 Contraseña<span className="req"></span>
                                </label>
                                <input type="password" required autoComplete="off" onChange={e=> this.setState({password:e.target.value})} />
                            </div>
                            <div className="field-wrap">
                                <label>
                                  REContraseña<span className="req"></span>
                                </label>
                                <input type="password" required autoComplete="off" onChange={e=> this.setState({repassword:e.target.value})} />
                            </div>

                            <button type="submit" className="btn btn-primary">Registrar</button>

                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Signup;
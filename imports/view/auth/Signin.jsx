import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import { Meteor } from 'meteor/meteor';

class Signin extends Component {
    constructor(props){
        super(props)
        this.state={
            username:'',
            password:''
        }
    }
    SubmitForlogin = (e) =>{
        e.preventDefault()
        const mthis = this
        Meteor.loginWithPassword(this.state.username,this.state.password,function(error){
            if (error)
                console.log(error)
            else{
                //mthis.props.history.push('/dashboard/home')
                window.location.replace('/dashboard/home')
            }
        })
    }
    render() {
        return (
            <form onSubmit = {this.SubmitForlogin} >
                <h1>Iniciar Seccion</h1>
                <div className="form-group">
                    <label htmlFor="exampleInputEmail1">Ingresar Email </label>
                    <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                        placeholder="Enter email" onChange={e=> this.setState({username:e.target.value})} autocomplete="off"/>
                    <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone
                        else.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Contraseña</label>
                    <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" onChange={e=> this.setState({password:e.target.value})} autocomplete="off" />
                </div>
                <div className="form-check">
                    <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
                    <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
                </div>
                <button type="submit" className="btn btn-primary">iniciar Seccion</button>
            </form>
        );
    }
}

export default Signin;
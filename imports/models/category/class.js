import { Class } from 'meteor/jagi:astronomy';
const categoryCollection = new Mongo.Collection('category',{idGeneration:'MONGO'}); //crear tablas en MDB

export const categoryClass = Class.create({   // modelo de la BD
  name: 'categoryClass',
  collection: categoryCollection,
  fields: {
    name:{
        type:String,
        validators: [{
            type: 'minLength',
            param: 3,
            resolveError({ name, param }) {
              return `el tamaño minimo de categoria tiene que ser ${param}`;
            }
          }]
    },
    description:{
        type:String,
        validators: [{
            type: 'minLength',
            param: 10,
            resolveError({ description, param }) {
              return `el tamaño minimo de descripcion tiene que ser ${param}`;
            }
          }]
    },
    active:{
      type:Boolean,
      default:true
  }
  }
});

if(Meteor.isServer){
    categoryClass.extend({
        fields: {
            user: String,
            
        },
        behaviors: {
            timestamp: {
              hasCreatedField: true,
              createdFieldName: 'createdAt',
              hasUpdatedField: true,
              updatedFieldName: 'updatedAt'
            }
          }
    })
}
import { Class } from 'meteor/jagi:astronomy';
const publicationsCollection = new Mongo.Collection('publications',{idGeneration:'MONGO'}); //crear tablas en MDB

export const publicationsClass = Class.create({   // modelo de la BD
  name: 'publicationsClass',
  collection: publicationsCollection,
  fields: {
    title:{
        type:String,

    },
    description:{
        type:String,
    },
    phone:{
        type:String,
    },
    
    startDate:{
        type:Date,
    },
    nameCategory:{
        type:String,
    },
    idfile:{
      type:String,
    },
    urlfile:{
      type:String,
    },
    active:{
      type:Boolean,
      default:true
    },
    idCategory:{
      type:Mongo.ObjectID
    },
    typepub:{
       type:String,
       default:'image'
    },
    username: String,
    created_view:{
      type:Date,
    }    
  }
});

if(Meteor.isServer){
  publicationsClass.extend({
      fields: {
          user: String,
          
      },
      behaviors: {
          timestamp: {
            hasCreatedField: true,
            createdFieldName: 'createdAt',
            hasUpdatedField: true,
            updatedFieldName: 'updatedAt'
          }
        }
  })
}
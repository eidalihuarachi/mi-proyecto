import {publicationsClass} from './class'
import UserFiles from '../files/class'
import {categoryClass} from '../category/class'
import {queryPublications} from './querys'


publicationsClass.extend({
    meteorMethods:{
        newPublication(form){
            try {
                if(Roles.userIsInRole(Meteor.userId(),"createpublication")){
                    const file = UserFiles.findOne({_id:form.image})
                    const URL = file.link()
                    const Indextext = file.mime.lastIndexOf('/') 
                    var newUrl = URL.replace(/.*\/\/[\/]+/,'')
                    const category = categoryClass.findOne({_id:new Meteor.Collection.ObjectID(form.category)})
                    this.title = form.title
                    this.description = form.description
                    this.phone = form.phone
                    this.nameCategory = category.name
                    this.idCategory = category._id
                    this.idfile = form.image
                    this.urlfile = newUrl
                    this.startDate = form.startDate
                    this.user = Meteor.userId()
                    this.username = Meteor.user().username
                    this.created_view = new Date()
                    this.typepub = file.mime.slice(0,Indextext)
                    this.save()
                    return"guardado correctamente"
                }else{
                    throw new Meteor.Error(403,'accseso denegado')
                }
                
            }catch (error){
                console.log(error)
                throw new Meteor.Error(403,error.reason)
            }
            
            //console.log(form)
            //console.log(this)
            //this.set(form)
            //this.title = form.title
            //this.description = form.description
            //this.phone = form.phone
            
            //this.startDate = form.startDate
          
            //console.log(this)

            //this.save()
            
        },
        updateStatePublication(){
            try{
                this.active = this.active?false:true
                return this.save()
            }catch(error){
                console.log(error)
                throw new Meteor.Error(403,error.reason)
            }
        }
    }
})
//Cl. se subcribira para ver la BD
//Meteor.publish('mispublicaciones',function(){
   // return publicationsClass.find()  // Select. todo de la collectons publications
//})

{Meteor.publish('publications',function(options,type){
    try {
           const subs = new queryPublications(options,this)
           return subs[type]()
    } catch (error) {
        console.log(error)
        this.stop()
        throw new Meteor.Error(403,error,reason)
    }
}) }
import Principal from '/imports/view/principal/Principal'
import Main from '/imports/view/principal/Main'
import Signin from '/imports/view/auth/Signin'
import Layout from '/imports/view/layout/Layout'
import createPublication from '/imports/view/publications/createPublication'
import userPublications from '/imports/view/publications/userPublications'
import Home from '/imports/view/home/Home'
import Signup from '/imports/view/auth/Signup'
import {createBrowserHistory} from 'history';


import CreateCategory from './CategoryRoutes'
import PublicationsRoutes from './PublicationsRoutes'
const history = createBrowserHistory();

export const routes = [
    {
        path:'/bienvenido',
        component:Principal,
        authenticated:false,
        routes:[
            {
                path:'/bienvenido/principal',
                component:Main
            },
            {
                path:'/bienvenido/login',
                component:Signin
            },
            {
                path:'/bienvenido/registro',
                component:Signup 
            }
            
        ]    
    },
    {
        path:'/dashboard',
        component:Layout,
        authenticated:true,
        routes: [
            {
                path:'/dashboard/home',
                component:Home 
            },
            {
                path:'/dashboard/create-publicacion',
                component:createPublication
            },
            {
                path:'/dashboard/user-publications',
                component:userPublications
            },
            ...PublicationsRoutes,
            ...CreateCategory
        ]
    },
    {
        path:'/otraruta',
        component:Layout,
        authenticated:true,
    }
    
]

function getAllRoutesAuthetincated(routes){
    let routesAuthetincate = []
    routes.forEach((value,index) => {
        if(JSON.stringify(value.authenticated ) === JSON.stringify(true)){
            routesAuthetincate.push(value.path)
            if(value.routes){
                value.routes.forEach((v,i) => {
                    routesAuthetincate.push(v.path)
                });
            }
        }
    });
    return routesAuthetincate
}

export const checkAuthUser = function(authenticated){
    //console.log('el usuario esta autenticado:'+authenticated)
    const path = history.location.pathname
    const isAuthenticatedPage = getAllRoutesAuthetincated(routes).includes(path)
    if(authenticated && isAuthenticatedPage){
        //console.log('el usuario puede ingresar a las paginas que necesitan autenticcacion')
        history.replace('/dashboard/home')
    }else if(!authenticated && isAuthenticatedPage){
        console.log('el usuario no esta logueado')
        history.replace('/')
        location.reload()
    }
}